﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices.ComTypes;
using System.Text;

namespace ChatServer
{
	public class ChatServer
	{
		public void StartServer()
		{
			IPHostEntry IpHost = Dns.GetHostEntry(Dns.GetHostName());
			IPAddress ipAddr = IpHost.AddressList[0];
			IPEndPoint localEndPoint = new IPEndPoint(ipAddr, 33333);

			Socket listener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
			try
			{
				listener.Bind(localEndPoint);
				listener.Listen(10);

				bool running = true;
				while (running)
				{
					Console.WriteLine("Waiting connection.....");

					Socket clientSocket = listener.Accept();
					byte[] bytes = new byte[1024];

					string data = null;
					bool runningData = true;
					while (runningData)
					{
						int numByte = clientSocket.Receive(bytes);
						data += Encoding.ASCII.GetString(bytes,0, numByte);

						if (data.IndexOf("Client msg:") > -1)
						{
							runningData = false;
						}
					}


					Console.WriteLine("Text received -> {0}", data);
					byte[] message = Encoding.ASCII.GetBytes("Server respond: You got me!!!");

					clientSocket.Send(message);
					clientSocket.Shutdown(SocketShutdown.Both);
					clientSocket.Close();
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
		}
	}
}
