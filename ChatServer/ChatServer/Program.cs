﻿using System;

namespace ChatServer
{
	class Program
	{
		static void Main(string[] args)
		{
			ChatServer chatServer = new ChatServer();
			chatServer.StartServer();
		}
	}
}
