﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ChatTek
{
	public class ChatClient
	{
		public IPHostEntry IpHost = Dns.GetHostEntry(Dns.GetHostName());
		public void StartClient()
		{
			IPAddress IpAddr = IpHost.AddressList[0];
			IPEndPoint localEndPoint = new IPEndPoint(IpAddr, 33333);

			try
			{
				bool runningClient = true;
				while (runningClient)
				{
					Socket sender = new Socket(IpAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

					sender.Connect(localEndPoint);
					string msg = "";
					msg = this.ClientMessage();
				

					if (msg == "End chat")
					{
						runningClient = false;
					}

					byte[] messageToSent = Encoding.ASCII.GetBytes("Client msg: " + msg);
					int byteSent = sender.Send(messageToSent);

					byte[] messageRecieved = new byte[1024];

					int byteRecv = sender.Receive(messageRecieved);
					Console.WriteLine("Message From server -> {0}", Encoding.ASCII.GetString(messageRecieved,0,byteRecv));
				
					sender.Shutdown(SocketShutdown.Both);
					sender.Close();
				}
			}
			catch (ArgumentNullException ane)
			{

				Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
			}

			catch (SocketException se)
			{

				Console.WriteLine("SocketException : {0}", se.ToString());
			}

			catch (Exception e)
			{
				Console.WriteLine("Unexpected exception : {0}", e.ToString());
			}
		}

		public string ClientMessage()
		{
			Console.WriteLine("Skriv besked: ");
			string msg = Console.ReadLine();
			return msg;
		}
	}
}
