﻿using System;

namespace ChatTek
{
	class Program
	{
		static void Main(string[] args)
		{
			ChatClient chatClient = new ChatClient();
			chatClient.StartClient();
		}
	}
}
